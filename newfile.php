<?php
/**
 * 功能：将zh-cn的资源文件生成到db.xls文件
 * 
 */
        error_reporting(E_ALL);
ini_set('display_errors',1);

define('G_ISCDIR','/usr/local/svconfig/iscconf');
define('G_VMDIR','/usr/local/svconfig/vmconf');
date_default_timezone_set('Asia/Chongqing');
define('ROOT',dirname(dirname(__FILE__)));
define('PATH',dirname(__FILE__));

require_once ROOT.'/Class/PHPExcel.php';
require_once ROOT.'/Class/PHPExcel/Writer/Excel5.php'; 

// 创建一个处理对象实例       
$objExcel = new PHPExcel();       
      
// 创建文件格式写入对象实例, uncomment       
$objWriter = new PHPExcel_Writer_Excel5($objExcel);      


$objExcel->setActiveSheetIndex(0);       
$objActSheet = $objExcel->getActiveSheet();       
      
//设置当前活动sheet的名称       
$objActSheet->setTitle('English');

//设置宽度，这个值和EXCEL里的不同，不知道是什么单位，略小于EXCEL中的宽度   
$objActSheet->getColumnDimension('A')->setWidth(20);    
$objActSheet->getColumnDimension('B')->setWidth(100);    
$objActSheet->getColumnDimension('C')->setWidth(100); 


//设置单元格的值     
$objActSheet->setCellValue('A1', '变量名称');    
$objActSheet->setCellValue('B1', '中文');    
$objActSheet->setCellValue('C1', '英文');


$GJS_OEMType = 'H3C';
$OEM = include PATH.'/zh-cn/OEM.php';
$type = &$OEM;
$arr = &$OEM;
//从第二行开始写人
$i=2;
//获取目录下的所有资源列表
$lang_list = getLangList(PATH.'/zh-cn/');
foreach ($lang_list as $list_v)
{
    $data_zh = require PATH.'/zh-cn/'.$list_v.'.php';
    $data_en = require PATH.'/en/'.$list_v.'.php';
    if(empty($data_zh)))
    {
        $objActSheet->setCellValue('A'.$i, $list_v);    
        $objActSheet->setCellValue('B'.$i, '');    
        $objActSheet->setCellValue('C'.$i, '');    
        $i++;
    }
    else
    {
        $list = getLang($list_v, $data_zh,$data_en);
        foreach ($list as $k=>$v)
        {
            $tmp = explode('|^c^|',$v);
            $objActSheet->setCellValue('A'.$i, $k);    
            $objActSheet->setCellValue('B'.$i, $tmp[0]);    
            $objActSheet->setCellValue('C'.$i, $tmp[1]);    
            $i++;
        }
    }
}

$outputFileName = PATH."/db_all.xls";
//到文件       
$objWriter->save($outputFileName);
echo 'finish,Total create record:'.$i;
function getLang($parent_,$arr_,$arr_en)
{
    $tmparr = array();
    foreach ($arr_ as $k=>$v)
    {
        $key = ($parent_)?$parent_.'|'.$k:$k;
        $enValue = $arr_en[$k];
        if(is_array($v) && !empty($v))
        {
            $tmparr2 = getLang($key, $v,$enValue);
            if(is_array($tmparr2) && !empty($tmparr2))
            {
                //$tmparr[]=$tmparr2;
                $tmparr = array_merge($tmparr,$tmparr2);
            }
        }
        else 
        {
            $tmparr[$key]=$v.'|^c^|'.$enValue;
        }
    }
    return $tmparr;
}

function getServType(){
	return is_dir(G_ISCDIR) ? 1 : 0;
}

function getLangList($path)
{
    $fp=opendir($path);
    $list = array();
    while(false!=$file=readdir($fp))
    {
        //列出所有文件并去掉'.'和'..'
        if($file!='.' && $file!='..')
        {
            //扩展名不需要
            $list[] = str_replace('.php','',$file);
        }
    }
    return $list;
}
