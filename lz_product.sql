-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2012 年 08 月 17 日 19:39
-- 服务器版本: 5.1.28
-- PHP 版本: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `lz_product`
--

-- --------------------------------------------------------

--
-- 表的结构 `pc_about`
--

DROP TABLE IF EXISTS `pc_about`;
CREATE TABLE IF NOT EXISTS `pc_about` (
  `desc` varchar(4000) NOT NULL,
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_about`
--

INSERT INTO `pc_about` (`desc`, `id`, `name`) VALUES
('<p>\r\n	<strong>adadadasdasd</strong><strong></strong>\r\n</p>', 1, 'adada');

-- --------------------------------------------------------

--
-- 表的结构 `pc_cate`
--

DROP TABLE IF EXISTS `pc_cate`;
CREATE TABLE IF NOT EXISTS `pc_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `icon` varchar(150) NOT NULL,
  `orders` tinyint(4) unsigned NOT NULL,
  `adddate` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `pc_cate`
--

INSERT INTO `pc_cate` (`id`, `name`, `icon`, `orders`, `adddate`) VALUES
(2, '测试1', '', 0, 0),
(3, '测试2', '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `pc_news`
--

DROP TABLE IF EXISTS `pc_news`;
CREATE TABLE IF NOT EXISTS `pc_news` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `desc` varchar(4000) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `adddate` int(11) unsigned NOT NULL,
  `hots` tinyint(1) unsigned NOT NULL COMMENT '是否推荐产品',
  `memo` varchar(300) NOT NULL COMMENT '产品优势',
  `cate_id` int(11) unsigned NOT NULL,
  `author` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_news`
--

INSERT INTO `pc_news` (`id`, `name`, `desc`, `icon`, `adddate`, `hots`, `memo`, `cate_id`, `author`) VALUES
(1, '测试标题1', '详细内容', './Uploads/1345230906.png', 1345230906, 1, '资讯简介', 2, '中国');

-- --------------------------------------------------------

--
-- 表的结构 `pc_news_cate`
--

DROP TABLE IF EXISTS `pc_news_cate`;
CREATE TABLE IF NOT EXISTS `pc_news_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_news_cate`
--

INSERT INTO `pc_news_cate` (`id`, `name`) VALUES
(1, '公司资讯'),
(2, '技术资料');

-- --------------------------------------------------------

--
-- 表的结构 `pc_products`
--

DROP TABLE IF EXISTS `pc_products`;
CREATE TABLE IF NOT EXISTS `pc_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `desc` varchar(300) NOT NULL,
  `icon` varchar(150) NOT NULL,
  `hots` tinyint(1) unsigned NOT NULL,
  `memo` varchar(255) NOT NULL,
  `adddate` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- 转存表中的数据 `pc_products`
--

INSERT INTO `pc_products` (`id`, `name`, `desc`, `icon`, `hots`, `memo`, `adddate`) VALUES
(1, '展位标题', '详细内容', './Uploads/1345231131.png', 0, '123', 1345231131);

-- --------------------------------------------------------

--
-- 表的结构 `pc_user`
--

DROP TABLE IF EXISTS `pc_user`;
CREATE TABLE IF NOT EXISTS `pc_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `pc_user`
--

INSERT INTO `pc_user` (`id`, `name`, `pwd`) VALUES
(1, 'selectersky', 'e10adc3949ba59abbe56e057f20f883e'),
(2, 'test', 'e10adc3949ba59abbe56e057f20f883e');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
