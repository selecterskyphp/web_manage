<?php
class ViewAction extends Action {

    public function index()
    {
        $type = isset($_GET['type'])?trim($_GET['type']):'';
        $id = isset($_GET['id'])?trim($_GET['id']):0;
        $mod = null;
        switch ($type)
        {
            case 'about':
                $mod = M('About');
                break;
            case 'news':
                $mod = M('News');
                break;
            case 'pro':
                $mod = M('Products');
                break;
        }
        if(!$mod || !is_numeric($id) || !$id)
        {
            exit();
        }
        $where = array();
        $data = $mod->getById($id);
        if($data)
        {
            $this->assign('type',$type);
            $this->assign('vo',$data);
            $this->display();
        }
    }

}

?>