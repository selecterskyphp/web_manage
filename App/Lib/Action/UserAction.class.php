<?php
class UserAction extends BasicAction {

    public function save()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create($_POST);
        $data['adddate'] = time();
        if(is_array($_POST['role_list']))
        {
            $data['role_list'] = implode(',', $_POST['role_list']);
        }
        else 
        {
            $data['role_list'] = '';
        }
        if(!$data['pwd'])
        {
            $this->error('密码不能为空');
        }
        if(strlen($data['name'])<1 || strlen($data['name'])>20)
        {
            $this->error('用户名必须是1-20字符');
        }
        if($data['pwd'] !== $_POST['__pwd2'])
        {
            $this->error('两次密码输入不一样');
        }
        $result = $mod->getByName($data['name']);
        if(is_array($result))
        {
            $this->error('用户已经存在');
        }
        $data['pwd'] = md5($data['pwd']);
        $data['updatedate']=time();
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function update()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create($_POST);
        
        if(is_array($_POST['role_list']))
        {
            $data['role_list'] = implode(',', $_POST['role_list']);
        }
        if(strlen($data['pwd'])>0)
        {
            if($data['pwd'] !== $_POST['__pwd2'])
            {
                $this->error('两次密码输入不一样');
            }
            $data['pwd'] = md5($data['pwd']);
        }
        else 
        {
            unset($data['pwd']);
        }
        if($data['id']<=2 || $data['name'] === C('admin'))
        {
            unset($data['role_list']);
        }
        $data['updatedate']=time();
        $result = $mod->data($data)->save();
        //var_dump($mod->getlastsql(),$data,$_POST['role_list'],$data['role_list']);
       // exit;
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function del()
    {
        $id = intval($_GET['id']);
        if($id<=2)
        {
            $this->error('系统内置管理不允许删除');
        }
        $mod = M(MODULE_NAME);
        $where=array();
        $where['id']=$id;
        $vo = $mod->where($where)->delete();
        if(!$vo)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
}

?>