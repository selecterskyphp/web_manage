<?php
class ApiAction extends Action {
    private $lang;
    public function __construct()
    {
        parent::__construct();
        $langArr = C('langArr');
        $lang = isset($_GET['lang'])?trim($_GET['lang']):'zh-cn';
        $lang = strtolower($lang);
        $lang = str_replace('_','-',$lang);
        if(!array_key_exists($lang,$langArr))
        {
            $result = array();
            $result['code'] = 1;
            $result['message'] = '语种不存在';
            $this->_out($result);
        }
        $this->lang = $lang;
    }

    /**
     * 获取栏目分类
     */
    public function getCate()
    {
        $mod = M('Cate');
        $where=array();
        $where['show']=1;
        $where['lang']=$this->lang;
        $list = $mod->order('orders asc,id desc')->where($where)->select();
       // var_dump($mod->getlastsql());
        $this->_out($list);
    }
    /**
     * 获取关于我们详细
     * @parms {integer} id 详细ID号
     */
    public function getAbout()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):1;
        $mod = M('About');
        $where = array();
        $where['id'] = $id;
        $where['show']=1;
        $list = $mod->where($where)->find();
        $this->_out($list);
    }
    /**
     * 获取新闻列表
     * @params {integer} id 信息分类ID
     * @params {integer} start 分页开始记录数  默认从第一条记录开始
     * @params {ineger} end  每页显示多少条记录 默认为-1； -1表示所有 0表示每页20条  其他整数表示具体的值
     */
    public function getNews()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):1;
        $start = isset($_GET['start'])?intval($_GET['start']):0;
        $end = isset($_GET['end'])?intval($_GET['end']):-1;
        $mod = M('News');
        $where= array();
        $where['cate_id'] = $id;
        $where['show']=1;
        $count = $mod->where($where)->count();
        if(-1===$end)
        {
            $end=$count;
        }
        else if(0 ===$end)
        {
            $end = 20;
        }
        $list = $mod->field("id,name,adddate,icon,updatedate")->where($where)->order('orders desc,id desc')->limit($start.','.$end)->select();
        $result = array('recordCount'=>$count,'list'=>$list);
        //var_dump($mod->getlastsql(),$start,$end);
        $this->_out($result);
    }
    /**
     * 获取新闻详细
     * @params {integer} id 要显示的新闻ID
     */
    public function getNewsDetail()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):1;
        $mod = M('News');
        $where= array();
        $where['id'] = $id;
        $where['show']=1;
        $list = $mod->where($where)->order('id desc')->find();
        $this->_out($list);
    }
    /**
     * 获取热点新闻 该接口目前没有使用
     */
    public function getHotNews()
    {
        $mod = M('News');
        $where = array();
        $where['hots'] = 1;
        $list = $mod->where($where)->find();
        $this->_out($list);
    }
    /**
     * 获取新闻分类列表
     */
    public function getNewsCate()
    {
        $mod = M('NewsCate');
        $where = array();
        $where['lang']=$this->lang;
        $list = $mod->where($where)->order('id desc')->select();
        $this->_out($list);
    }
    /**
     * 获取图片记录列表
     * @params {integer} id 分类ID
     * @params {integer} start 分页开始记录数  默认从第一条记录开始
     * @params {ineger} end  每页显示多少条记录 默认为-1； -1表示所有 0表示每页20条  其他整数表示具体的值
     */
    public function getProducts()
    {
        $mod = M('Products');
        $start = isset($_GET['start'])?intval($_GET['start']):0;
        $end = isset($_GET['end'])?intval($_GET['end']):-1;
        $id = isset($_GET['id'])?intval($_GET['id']):1;
        $where= array();
        $where['cate_id'] = $id;
        $where['show']=1;
        $count = $mod->where($where)->count();
        if(-1===$end)
        {
            $end=$count;
        }
        else if(0 ===$end)
        {
            $end = 20;
        }
        $list = $mod->field("id,name,adddate,icon,updatedate")->where($where)->order('orders desc,id desc')->limit($start.','.$end)->select();
        $result = array('recordCount'=>$count,'list'=>$list);
        //var_dump($mod->getlastsql());
        $this->_out($result);
    }
    /**
     * 获取热点图片列表 该接口目前没有使用
     */
    public function getHotProducts()
    {
        $mod = M('Products');
        $where = array();
        $where['hots'] = 1;
        $list = $mod->where($where)->find();
        $this->_out($list);
    }
    /**
     * 获取图片详细信息
     * @params {integer} id 要显示的图片ID
     */
    public function getProDetail()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):1;
        $mod = M('Products');
        $where= array();
        $where['id'] = $id;
        $where['show']=1;
        $list = $mod->where($where)->order('id desc')->find();
        $this->_out($list);
    }
    /**
     * 保存留言信息
     * @params {string} content留言内容 必须是5-300字以内 
     */
    public function saveMessage()
    {
        $desc = isset($_POST['content'])?addslashes($_POST['content']):'';
        $result = array();
        if(strlen($desc)<5 || strlen($desc)>300)
        {
            $result['code'] = 1;
            $result['message'] = '字符数量必须为5-300字符';
            $this->_out($result);
        }
        $mod = M('Message');
        $data = array();
        $data['desc'] = $desc;
        $data['adddate'] = time();
        $re = $mod->data($data)->add();
        if($re)
        {
            $result['code'] = 0;
        }
        else 
        {
            $result['code'] = 2;
            $result['message'] = '写入数据失败';
        }
        $this->_out($result);
    }
    /**
     * 检查更新
     * $params {integer} id 更新包CODE值
     */
    public function update()
    {
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $mod = M('Update');
        $where = array();
        $where['code'] = array('gt',$id);
        $where['show'] = 1;
        $re = $mod->where($where)->order('code desc')->limit(1)->find();
        //有更新
        if($re && !empty($re))
        {
            $this->_out($re);
        }
        else
        {
            $this->_out(null,'',1);
        }
    }
    /**
     * 内部方法，返回数据 目前支持JSON和PJSON两种格式
     * $params $list 返回数据
     * $parmas $info 返回额外的提示信息
     * $params $status 状态 0为操作成功 其他值为错误码
     */
    private function _out($list,$info='',$status=0)
    {
        $callback = $_GET['callback'];
        if(isset($callback))
        {
            die($callback.'('.json_encode($list).')');
        }
        else
        {
            $this->ajaxReturn($list,$info,$status);
        }
    }
    /**
     * 测试
     */
    public function test()
    {
        echo time();
    }

}

?>
