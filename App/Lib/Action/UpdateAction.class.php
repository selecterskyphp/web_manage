<?php
class UpdateAction extends BasicAction {

    public function save()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create();
        if('' === $data['name'] || strlen($data['name'])>20)
        {
            $this->error('版本号在1-20个字符范围内');
        }
        if('' === $data['apk_name'] || strlen($data['apk_name'])>150)
        {
            $this->error('安装包在1-150个字符范围内');
        }
        $file = $this->apk_path.'/'.$data['apk_name'];
        if(!is_readable($file))
        {
            $this->error('安装包文件在路径'.$file.'不存在，请先上传');
        }
        $max = $mod->max("code");
        $data['code'] = $max+1;
        $data['adddate'] = time();
        $data['updatedate']=time();
        $data['show'] = 0;
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('');
        }
    }
    public function update()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create($_POST);
        if('' === $data['apk_name'] || strlen($data['apk_name'])>150)
        {
            $this->error('安装包在1-150个字符范围内');
        }
        $file = $this->apk_path.'/'.$data['apk_name'];
        if(!is_readable($file))
        {
            $this->error('安装包文件在路径'.$file.'不存在，请先上传');
        }
        $data['updatedate']=time();
        $result = $mod->data($data)->save();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U('Index/index'));
            $this->success('');
        }
    }
    public function doShow()
    {
        $mod = M(MODULE_NAME);
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $show = isset($_GET['show'])?intval($_GET['show']):0;
        $data = array('show'=>$show);
        $mod->where('id='.$id)->save($data);
        $this->assign('jumpUrl',U('Index/index'));
        $this->success('');
    }
}

?>