<?php
class ProductsAction extends BasicAction {

public function save()
    {
        $mod = M(MODULE_NAME);
        $id = isset($_POST['id'])?intval($_POST['id']):0;
	    $name = isset($_POST['name'])?trim($_POST['name']):'';
	    $memo = isset($_POST['memo'])?trim($_POST['memo']):'';
	    $desc = isset($_POST['desc'])?str_replace('\\','',$_POST['desc']):'';
	    $orders = isset($_POST['orders'])?intval($_POST['orders']):0;
	    $cate_id = isset($_POST['cate_id'])?intval($_POST['cate_id']):0;
	    $size = 100 * 1024;
	    $icon = '';
	    if(isset($_FILES['icon']['name']) && $_FILES['icon']['size']>0)
	    {
	        $type = strtolower($_FILES['icon']['type']);
	        if(false === strpos($type, 'png') && false === strpos($type, 'jpg') && false === strpos($type, 'gif') && false === strpos($type, 'jpeg'))
	        {
	            $this->error('不支持该文件类型，当前只支持png/jpg/jpeg/gif');
	        }
	        if($_FILES['icon']['size'] > $size)
	        {
	            $this->error('上传的文件太大，不能超过100K');
	        }
	        $file = './Uploads/'.time().'.png';
	        $result = @move_uploaded_file($_FILES['icon']['tmp_name'], $file);
	        if(!$result)
	        {
	            $this->error('上传文件失败');
	        }
	        $icon = $file;
	    }
	    if('' === $name || strlen($name)>255)
	    {
	        $this->error('图片名称为空');
	    }
        if(0 == $cate_id)
	    {
	        $this->error('图片类型必须选择');
	    }
	    $data = array();
	    $data['name'] = str_replace('\'', '', $name);
	    $data['memo'] = str_replace('\'', '', $memo);
	    $data['desc'] = str_replace('\'', '', $desc);
	    $data['orders'] = $orders;
	    $data['updatedate']=time();
	    $data['cate_id'] = $cate_id;
	    if($id == 0 ||  ($id>0 && strlen($icon)>5))
	    {
	        $data['icon'] = $icon;
	    }
	    if(0 === $id)
	    {
    	    $data['adddate'] = time();
	        $result = $mod->data($data)->add();
	    }
	    else 
	    {
	        $where= array();
	        $where['id']=$id;
	        $result = $mod->data($data)->where($where)->save();
	    }
	    if(!$result)
	    {
	        $this->error('');
	    }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    
    public function doPush()
    {
    	import("@.Vendor.BaiduPush.Channel");
    	$id = isset($_GET['id'])?intval($_GET['id']):0;
    	$mod = M('Products');
    	$where=array();
        $where['id']=$id;
        $vo = $mod->where($where)->find();
        //将状态设置为已经推送
        if($vo)
        {
        	 $data = array();
        	 $data['isPush'] = 1;
        	 $mod->where($where)->data($data)->save();
        }
    	
    	//$this->right_output
    	$apiKey = 'SXSenxfwi9U5QmcnykuOPSyC';
    	$secretKey = 'Y1bumRY0xRev7TfVFtkophFS8xzaON98';
    	$channel = new Channel ( $apiKey, $secretKey ) ;
		//推送消息到某个user，设置push_type = 1; 
		//推送消息到一个tag中的全部user，设置push_type = 2;
		//推送消息到该app中的全部user，设置push_type = 3;
		$push_type = 3; //推送单播消息
		//$optional[Channel::USER_ID] = $user_id; //如果推送单播消息，需要指定user
		//$optional[Channel::TAG_NAME] = "xxxx";  //如果推送tag消息，需要指定tag_name

		//指定发到android设备
		$optional[Channel::DEVICE_TYPE] = 3;
		//指定消息类型为通知
		$optional[Channel::MESSAGE_TYPE] = 1;
		//通知类型的内容必须按指定内容发送，示例如下：
		$message = '{ 
				"title": "最新产品(点击查看详细)",
				"description": "'.$vo['name'].'",
				"notification_basic_style":7,
				"open_type":0,
				"custom_content":{"id":"'.$id.'"}}';
		
		$message_key = "msg_key";
	    $ret = $channel->pushMessage ( $push_type, $message, $message_key, $optional ) ;
	    if ( false === $ret )
	    {
	    	$this->error('推送失败，错误码：'.$channel->errno().':'.$channel->errmsg());
	    }
	    else
	    {
	    	$this->assign('jumpUrl',U(MODULE_NAME.'/index'));
	        $this->success('');
	    }
    }
}

?>