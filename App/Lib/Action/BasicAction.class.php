<?php
class BasicAction extends Action {
    protected $app_arr=array();
    protected $action_arr=array();
    protected $lanmu_arr = array();
    protected $role_arr = array();
    protected $lang_arr = array();
    protected $userid = 0;
    protected $username = 0;
    protected $root = null;
    protected $apk_path = null;
    public function __construct()
    {
        parent::__construct();
        $this->root = dirname(dirname(dirname(dirname(__FILE__))));
        $this->apk_path = dirname($this->root).'/app/android';
        if(!$_SESSION['login']['id'] && false === strpos(ACTION_NAME,'login'))
        {
            $this->redirect("Index/login");
            exit;
        }
        $this->userid = $_SESSION['login']['id'];
        $this->username = $_SESSION['login']['name'];
        $this->role_arr = explode(',', $_SESSION['login']['role_list']);
        $this->app_arr = C('appArr');
        $this->lanmu_arr = C('lanmuArr');
        $this->action_arr = C('actionArr');
        $this->lang_arr = C('langArr');
        $this->assign('app_arr',$this->app_arr);
        $this->assign('lanmu_arr',$this->lanmu_arr);
        $this->assign('action_arr',$this->action_arr);
        $this->assign('lang_arr',$this->lang_arr);
        if($this->userid == 1 || $this->username == C('admin'))
        {
            $this->role_arr = array();
            foreach($this->app_arr as $k=>$v)
            {
                $this->role_arr[] = $k . "_index";
                foreach($this->action_arr as $kk=>$vv)
                {
                    $this->role_arr[] = $k . "_" . $kk;
                }
            }
        }
        $this->assign('role_arr',$this->role_arr);
        
        $this->assign('MODULE_NAME',MODULE_NAME);
        $this->assign('ACTION_NAME',ACTION_NAME);
        $this->assign('login',$_SESSION['login']);
        
        if(MODULE_NAME != 'Index' && !in_array(MODULE_NAME . '_'.ACTION_NAME, $this->role_arr) && ACTION_NAME != 'save' && ACTION_NAME != 'update')
        {
            $this->error('对不起，您没有操作权限');
        }
        if(MODULE_NAME != 'Index')
        {
            $this->assign('title',$this->app_arr[MODULE_NAME].' - '.$this->action_arr[ACTION_NAME]);
            if(ACTION_NAME == 'index')
            {
                $mod = M(MODULE_NAME);
                if(MODULE_NAME == 'Cate')
                {
                    $order = 'orders asc,id desc';
                }
                else if(MODULE_NAME == 'News' || MODULE_NAME == 'Products')
                {
                    $order = 'orders desc,id desc';
                }
                else
                {
                    $order = 'id desc';
                }
                $where = 'id>0';
                $keyword = isset($_GET['keyword'])?trim($_GET['keyword']):'';
                $cate_id = isset($_GET['cate_id'])?intval($_GET['cate_id']):0;
                if(strlen($keyword)>0)
                {
                    $where .= ' and name like \'%'.$keyword.'%\'';
                    $this->assign('keyword',$keyword);
                }
                if($cate_id>0)
                {
                    $where .= ' and cate_id=\''.$cate_id.'\'';
                    $this->assign('cate_id',$cate_id);
                }
                $count = $mod->where($where)->count();
                import("ORG.Util.Page");// 导入分页类
                $page= new Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数
                $show = $page->show();// 分页显示输出
                $list = $mod->where($where)->order($order)->limit($page->firstRow.','.$page->listRows)->select();
                if(MODULE_NAME == 'News' || MODULE_NAME == 'Products')
                {
                    $mod = M(MODULE_NAME.'Cate');
                    for($i=0;$i<count($list);$i++)
                    {
                        $where = array();
                        $where['id'] = $list[$i]['cate_id'];
                        $name = $mod->where($where)->getField('name');
                        $list[$i]['catename'] = $name;
                    }
                    $catelist = $mod->select();
                    $this->assign('catelist',$catelist);
                }
               
                if(MODULE_NAME == 'NewsCate' || MODULE_NAME == 'ProductsCate')
                {
                    for($i=0;$i<count($list);$i++)
                    {
                        $list[$i]['langname'] = $this->lang_arr[$list[$i]['lang']];
                    }
                }
                if(MODULE_NAME == 'Cate')
                {
                    for($i=0;$i<count($list);$i++)
                    {
                        $modname = '';
                        switch ($list[$i]['model_type'])
                        {
                            case 1:
                                $modname = 'About';
                                break;
                            case 2:
                                $modname = 'NewsCate';
                                break;
                            case 3:
                                $modname = 'ProductsCate';
                                break;
                        }
                        $mod = M($modname);
                        $where = array();
                        $type_arr =C('model_type');
                        $where['id'] = $list[$i]['model_id'];
                        $name = $mod->where($where)->getField('name');
                        $list[$i]['modelname'] = $type_arr[$list[$i]['model_type']];
                        if($list[$i]['model_type']!=4)
                        {
                            $list[$i]['modelidname'] = $name;
                        }
                        else
                        {
                            $list[$i]['modelidname'] = $list[$i]['url'];
                        }
                        $list[$i]['langname'] = $this->lang_arr[$list[$i]['lang']];
                    }
                }
                $this->assign('list',$list);
                $this->assign('page',$show);
               // $this->display();
            }
        }
        else 
        {
            $this->assign('title','后台管理首页');
           // $this->display();
        }
    }
    public function login()
    {
        $this->display();
    }
    
    public function dologin()
    {
        $user = $_POST['user'];
        $pass = $_POST['pass'];
        if(!$user)
        {
            $this->error('用户名或密码错误');
        }
        $mod = M('user');
        $where = array();
        $where['name'] = $user;
        $where['pwd'] = md5($pass);
        $result = $mod->where($where)->find();
        if(false === $result)
        {
            $this->error('登录失败用户或密码错误');
        }
        $data = array();
        $data['lastdate'] = time();
        $data['ip'] = get_client_ip();
        $where = array();
        $where['id'] = $result['id'];
        $mod->data($data)->where($where)->save();
        $_SESSION['login']=$result;
        $this->redirect('Index/index');
    }
    
    public function loginout()
    {
        unset($_SESSION['login']);
        $this->redirect('Index/login');
    }
    
    public function edit()
    {
        $id = intval($_GET['id']);
        $mod = M(MODULE_NAME);
        $where=array();
        $where['id']=$id;
        $vo = $mod->where($where)->find();
        if(false === $vo)
        {
            $this->error('你操作的记录不存在');
        }
        $this->assign('vo',$vo);
        if(MODULE_NAME == 'News' || MODULE_NAME == 'Products')
        {
            $mod = M(MODULE_NAME.'Cate');
            $catelist = $mod->select();
            $this->assign('catelist',$catelist);
        }
        if(MODULE_NAME == 'Cate')
        {
            $mod = M('About');
            $aboutlist = $mod->field('id,name')->select();
            $mod = M('NewsCate');
            $newscate = $mod->field('id,name')->select();
            $mod = M('ProductsCate');
            $productscate = $mod->field('id,name')->select();
           // var_dump($aboutlist,$newscate);
            $this->assign('aboutlist',$aboutlist);
            $this->assign('newscate',$newscate);
            $this->assign('productscate',$productscate);
        }
        $this->assign('action',U(MODULE_NAME.'/update'));
        $this->display();
    }
    public function add()
    {
        if(MODULE_NAME == 'News' || MODULE_NAME == 'v' || MODULE_NAME == 'Products')
        {
            $mod = M(MODULE_NAME.'Cate');
            $catelist = $mod->select();
            $this->assign('catelist',$catelist);
        }
        if(MODULE_NAME == 'Cate')
        {
            $mod = M('About');
            $aboutlist = $mod->field('id,name')->select();
            $mod = M('NewsCate');
            $newscate = $mod->field('id,name')->select();
            $mod = M('ProductsCate');
            $productscate = $mod->field('id,name')->select();
           // var_dump($aboutlist,$newscate);
            $this->assign('aboutlist',$aboutlist);
            $this->assign('newscate',$newscate);
            $this->assign('productscate',$productscate);
        }
        $this->assign('action',U(MODULE_NAME.'/save'));
        
        $this->display('edit');
    }
    public function save()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create($_POST);
        $data['adddate'] = time();
        $data['updatedate'] = time();
        if(isset($data['desc']))
        {
            $data['desc'] = str_replace('\\', '', $data['desc']);
        }
        $result = $mod->data($data)->add();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function update()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create($_POST);
        $data['updatedate']=time();
        $result = $mod->data($data)->save();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function del()
    {
        $id = intval($_GET['id']);
        if(MODULE_NAME == 'About' || MODULE_NAME == 'NewsCate' || MODULE_NAME == 'ProductsCate')
        {
            $type = 1;
            switch (MODULE_NAME)
            {
                case 'About':
                    $type =1;
                    break;
                case 'NewsCate':
                    $type = 2;
                    break;
                case 'ProductsCate':
                    $type = 3;
                    break;
            }
            $where = array();
            $where['model_type'] = $type;
            $where['model_id'] = $id;
            $mod = M('Cate');
            $result = $mod->where($where)->getField('name');
            if($result)
            {
                $this->error('栏目 <b>'.$result.'</b> 指定到这条记录，删除前请先修改此栏目的设置');
            }
        }
        $mod = M(MODULE_NAME);
        $where=array();
        $where['id']=$id;
        $vo = $mod->where($where)->delete();
        if(MODULE_NAME == 'NewsCate')
        {
            $mod = M('News');
            $where = array();
            $where['cate_id'] = $id;
            $mod->where($where)->delete();
        }
        if(MODULE_NAME == 'ProductsCate')
        {
            $mod = M('Products');
            $where = array();
            $where['cate_id'] = $id;
            $mod->where($where)->delete();
        }
        if(!$vo)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function hot()
    {
        $id = intval($_GET['id']);
        $mod = M(MODULE_NAME);
        $where=array();
        $where['id']=$id;
        $data = array();
        $data['hots'] = 0;
        $mod->where($where)->data($data)->save();
        $data['hots'] = 1;
        $result = $mod->where($where)->data($data)->save();
        if(!$result)
        {
            $this->error('');
        }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    
    public function doShow()
    {
        $mod = M(MODULE_NAME);
        $id = isset($_GET['id'])?intval($_GET['id']):0;
        $show = isset($_GET['show'])?intval($_GET['show']):0;
        $data = array('show'=>$show);
        $mod->where('id='.$id)->save($data);
        $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
        $this->success('');
    }
}

?>