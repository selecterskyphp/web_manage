<?php
class CateAction extends BasicAction {

    public function update()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create();
	    if('' === $data['name'] || strlen($data['name'])>20)
	    {
	        $this->error('栏目名称为空');
	    }
	    if($data['model_type']!=4)
	    {
            $data['model_id'] = intval($_POST['__model_id_'.$data['model_type']]);
            if(!$data['model_id'])
            {
                $this->error('模型ID必须选择或输入，如果没有可能的模型，请先添加');
            }
	    }
	    else
	    {
	        $data['model_id'] =0 ;
	        $data['url'] = $_POST['__model_id_'.$data['model_type']];
	        if(false === strpos(strtoupper($data['url']),'HTTP://') && false === strpos(strtoupper($data['url']),'HTTPS://'))
	        {
	            $this->error('网址输入错误');
	        }
	    }
        $data['updatedate']=time();
	    $result = $mod->data($data)->save();
        if(!$result)
	    {
	        $this->error('');
	    }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    public function edit()
    {
        $this->assign('iconlist',$this->getIconList());
        parent::edit();
    }
   public function add()
    {
        $iconlist = $this->getIconList();
        $vo['icon'] = $iconlist[0];
        $this->assign('vo',$vo);
        $this->assign('iconlist',$iconlist);
        parent::add();
    }
    public function save()
    {
        $mod = M(MODULE_NAME);
        $data = $mod->create();
	    if('' === $data['name'] || strlen($data['name'])>20)
	    {
	        $this->error('栏目名称为空');
	    }
	    if($data['model_type']!=4)
        {
            $data['model_id'] = intval($_POST['__model_id_'.$data['model_type']]);
            if(!$data['model_id'])
            {
                $this->error('模型ID必须选择或输入，如果没有可能的模型，请先添加');
            }
        }
        else
        {
            $data['model_id'] =0 ;
            $data['url'] = $_POST['__model_id_'.$data['model_type']];
            if(false === strpos("HTTP://",strtoupper($data['url'])) || false === strpos("HTTPS://",strtoupper($data['url'])))
            {
                $this->error('网址输入错误');
            }
        }
        $data['adddate'] = time();
        $data['updatedate']=time();
        $data['show'] = 0;
        $result = $mod->data($data)->add();
	    if(!$result)
	    {
	        $this->error('');
	    }
        else 
        {
            $this->assign('jumpUrl',U(MODULE_NAME.'/index'));
            $this->success('');
        }
    }
    private function getIconList()
    {
        $path = dirname(dirname(dirname(dirname(__FILE__)))) . '/icon';
        $fp=opendir($path);
        $list = array();
        while(false!=$file=readdir($fp))
        {
            //列出所有文件并去掉'.'和'..'
            if($file!='.' && $file!='..')
            {
                $list[] = $file;
            }
        }
        return $list;
    }
    
}

?>