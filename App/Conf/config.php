<?php
$database = require ('./config.php');//数据库配置
//框架相关配置
$config= array(
//'SHOW_PAGE_TRACE'        =>true,
    //支持模型
    'model_type'=>array(
        '1'=>'单页模型',
        '2'=>'资讯模型',
        '3'=>'图片模型',
        '4'=>'网址模型'
    ),
    //栏目列表
    'appArr'=>array(
    'Cate'=>'栏目分类',
    'About'=>'单页模型',
    'NewsCate'=>'资讯分类',
    'News'=>'资讯模型',
    'ProductsCate'=>'图片分类',
    'Products'=>'图片模型',
    'User'=>'用户管理',
    'Message'=>'意见反馈',
    'Update'=>'更新'
    ),
    //操作数列表
    'actionArr'=>array(
    'add'=>'增加',
    'edit'=>'编辑',
    'del'=>'删除',
    'hot'=>'推荐',
    'doShow'=>'发布',
    'doPush' =>'推送'
    ),
    //栏目页面名称
    'lanmuArr'=>array('Cate','About','NewsCate','News','ProductsCate','Products','User','Message'),
    //超级管理员
    'admin'=>'test',
    //支持语种
    'langArr'=>array('zh-cn'=>'中文','en'=>'英文')
);
return array_merge($database, $config);
?>